<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $address = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $image = null;

    #[ORM\Column(length: 255)]
    private ?string $siret = null;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: User::class)]
    private Collection $recruteurs;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: Annonce::class)]
    private Collection $owned_by;

    public function __construct()
    {
        $this->recruteurs = new ArrayCollection();
        $this->owned_by = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getRecruteurs(): Collection
    {
        return $this->recruteurs;
    }

    public function addRecruteur(User $recruteur): self
    {
        if (!$this->recruteurs->contains($recruteur)) {
            $this->recruteurs->add($recruteur);
            $recruteur->setCompany($this);
        }

        return $this;
    }

    public function removeRecruteur(User $recruteur): self
    {
        if ($this->recruteurs->removeElement($recruteur)) {
            // set the owning side to null (unless already changed)
            if ($recruteur->getCompany() === $this) {
                $recruteur->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Annonce>
     */
    public function getOwnedBy(): Collection
    {
        return $this->owned_by;
    }

    public function addOwnedBy(Annonce $ownedBy): self
    {
        if (!$this->owned_by->contains($ownedBy)) {
            $this->owned_by->add($ownedBy);
            $ownedBy->setCompany($this);
        }

        return $this;
    }

    public function removeOwnedBy(Annonce $ownedBy): self
    {
        if ($this->owned_by->removeElement($ownedBy)) {
            // set the owning side to null (unless already changed)
            if ($ownedBy->getCompany() === $this) {
                $ownedBy->setCompany(null);
            }
        }

        return $this;
    }
}
